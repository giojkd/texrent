<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCmsUsersColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->string('surname')->nullable();
            $table->string('company')->nullable();
            $table->string('telephone')->nullable();
            $table->string('facebook_id')->nullable();
        });
    }

/*
Nome, Cognome, Azienda, Telefono, Email (tutti obbligatori), Password
Facebook Login
*/

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            //
        });
    }
}
