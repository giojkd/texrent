<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use GuzzleHttp\Client;

class AdminPushNotifications1Controller extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "title";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = false;
		$this->table = "push_notifications";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Title","name"=>"title"];
		$this->col[] = ["label"=>"Content","name"=>"content"];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Title','name'=>'title','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
		$this->form[] = ['label'=>'Content','name'=>'content','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ["label"=>"Title","name"=>"title","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
		//$this->form[] = ["label"=>"Content","name"=>"content","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
		//$this->form[] = ["label"=>"Delivery To","name"=>"delivery_to","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
		# OLD END FORM

		/*
		| ----------------------------------------------------------------------
		| Sub Module
		| ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
		|
		*/
		$this->sub_module = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Action Button / Menu
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
		| @icon        = Font awesome class icon. e.g : fa fa-bars
		| @color 	   = Default is primary. (primary, warning, succecss, info)
		| @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
		|
		*/
		$this->addaction = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Button Selected
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @icon 	   = Icon from fontawesome
		| @name 	   = Name of button
		| Then about the action, you should code at actionButtonSelected method
		|
		*/
		$this->button_selected = array();


		/*
		| ----------------------------------------------------------------------
		| Add alert message to this module at overheader
		| ----------------------------------------------------------------------
		| @message = Text of message
		| @type    = warning,success,danger,info
		|
		*/
		$this->alert        = array();



		/*
		| ----------------------------------------------------------------------
		| Add more button to header button
		| ----------------------------------------------------------------------
		| @label = Name of button
		| @url   = URL Target
		| @icon  = Icon from Awesome.
		|
		*/
		$this->index_button = array();



		/*
		| ----------------------------------------------------------------------
		| Customize Table Row Color
		| ----------------------------------------------------------------------
		| @condition = If condition. You may use field alias. E.g : [id] == 1
		| @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
		|
		*/
		$this->table_row_color = array();


		/*
		| ----------------------------------------------------------------------
		| You may use this bellow array to add statistic at dashboard
		| ----------------------------------------------------------------------
		| @label, @count, @icon, @color
		|
		*/
		$this->index_statistic = array();



		/*
		| ----------------------------------------------------------------------
		| Add javascript at body
		| ----------------------------------------------------------------------
		| javascript code in the variable
		| $this->script_js = "function() { ... }";
		|
		*/
		$this->script_js = NULL;


		/*
		| ----------------------------------------------------------------------
		| Include HTML Code before index table
		| ----------------------------------------------------------------------
		| html code to display it before index table
		| $this->pre_index_html = "<p>test</p>";
		|
		*/
		$this->pre_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include HTML Code after index table
		| ----------------------------------------------------------------------
		| html code to display it after index table
		| $this->post_index_html = "<p>test</p>";
		|
		*/
		$this->post_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include Javascript File
		| ----------------------------------------------------------------------
		| URL of your javascript each array
		| $this->load_js[] = asset("myfile.js");
		|
		*/
		$this->load_js = array();



		/*
		| ----------------------------------------------------------------------
		| Add css style at body
		| ----------------------------------------------------------------------
		| css code in the variable
		| $this->style_css = ".style{....}";
		|
		*/
		$this->style_css = NULL;



		/*
		| ----------------------------------------------------------------------
		| Include css File
		| ----------------------------------------------------------------------
		| URL of your css each array
		| $this->load_css[] = asset("myfile.css");
		|
		*/
		$this->load_css = array();


	}


	/*
	| ----------------------------------------------------------------------
	| Hook for button selected
	| ----------------------------------------------------------------------
	| @id_selected = the id selected
	| @button_name = the name of button
	|
	*/
	public function actionButtonSelected($id_selected,$button_name) {
		//Your code here

	}


	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate query of index result
	| ----------------------------------------------------------------------
	| @query = current sql query
	|
	*/
	public function hook_query_index(&$query) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate row of index table html
	| ----------------------------------------------------------------------
	|
	*/
	public function hook_row_index($column_index,&$column_value) {
		//Your code here
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before add data is execute
	| ----------------------------------------------------------------------
	| @arr
	|
	*/
	public function hook_before_add(&$postdata) {
		//Your code here

		/*

		curl -XPOST https://management-api.wonderpush.com/v1/deliveries?accessToken="YOUR_ACCESS_TOKEN" \
		-d targetSegmentIds=@ALL \
		-d notification=''

		*/

		$client = new Client();

		$notificationIcon = 'https://cdn.by.wonderpush.com/upload/01dl5vqh9t1jcbr2/6fc557e9cee6fab1f169672733bc21061eef21d5';

		$notification = json_decode('{"alert": {"text": "Example push notification", "ios":{"attachments":[{"url":"https://download.blender.org/peach/trailer/trailer_iphone.m4v"}]}, "android":{"bigTitle":"cane"}, "web":{"image":"https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Example.svg/2000px-Example.svg.png"}}}',1);

			$notification['alert']['android']['title'] = $postdata['title'];
			$notification['alert']['android']['text'] = $postdata['content'];

			$notification['alert']['android']['bigLargeIcon'] = $notificationIcon;
			$notification['alert']['android']['smallIcon'] = $notificationIcon;
			$notification['alert']['android']['largeIcon'] = $notificationIcon;


		$response = $client->post('https://management-api.wonderpush.com/v1/deliveries?accessToken=MGE3ZThmZTBkYjM4NjEzNDRiMjYwOWQyMDA4ZWExYmYwMDdkOGY5MGY5MGE0MGZjZWZhZGZkMGU4YjJhZTBjYQ', [
			'form_params' => [
				'targetSegmentIds' => '@ALL',
				'notification' => $notification
			],
		]);

/*
		exit;

		error_reporting(0);
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 0);

		include_once(app_path() . '/libraries/wonderpush/init.php');

		$wonderpush = new \WonderPush\WonderPush('MGE3ZThmZTBkYjM4NjEzNDRiMjYwOWQyMDA4ZWExYmYwMDdkOGY5MGY5MGE0MGZjZWZhZGZkMGU4YjJhZTBjYQ', '01dl5vqh9t1jcbr2');




		$response = $wonderpush->deliveries()->prepareCreate()
		->setTargetSegmentIds('@ALL')
		->setNotification(
			\WonderPush\Obj\Notification::_new()
			->setAlert(
				\WonderPush\Obj\NotificationAlert::_new()
				->setTitle($postdata['title'])
				->setText($postdata['content'])
				)

				)
				->execute()
				->checked();



				exit;
*/
			}

			/*
			| ----------------------------------------------------------------------
			| Hook for execute command after add public static function called
			| ----------------------------------------------------------------------
			| @id = last insert id
			|
			*/
			public function hook_after_add($id) {
				//Your code here

			}

			/*
			| ----------------------------------------------------------------------
			| Hook for manipulate data input before update data is execute
			| ----------------------------------------------------------------------
			| @postdata = input post data
			| @id       = current id
			|
			*/
			public function hook_before_edit(&$postdata,$id) {
				//Your code here

			}

			/*
			| ----------------------------------------------------------------------
			| Hook for execute command after edit public static function called
			| ----------------------------------------------------------------------
			| @id       = current id
			|
			*/
			public function hook_after_edit($id) {
				//Your code here

			}

			/*
			| ----------------------------------------------------------------------
			| Hook for execute command before delete public static function called
			| ----------------------------------------------------------------------
			| @id       = current id
			|
			*/
			public function hook_before_delete($id) {
				//Your code here

			}

			/*
			| ----------------------------------------------------------------------
			| Hook for execute command after delete public static function called
			| ----------------------------------------------------------------------
			| @id       = current id
			|
			*/
			public function hook_after_delete($id) {
				//Your code here

			}



			//By the way, you can still create your own method in here... :)


		}
