<?php namespace App\Http\Controllers;

use Session;
#use Request;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Cartalyst\Stripe\Stripe;

class AdminReservationsController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "id";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = false;
		$this->table = "reservations";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"ID","name"=>"id"];
		$this->col[] = ["label"=>"Frame","name"=>"frames_id","join"=>"frames,name"];
		$this->col[] = ["label"=>"User","name"=>"cms_users_id","join"=>"cms_users,name"];
		$this->col[] = ["label"=>"Status","name"=>"status_id","join"=>"reservation_statuses,name"];
		$this->col[] = ["label"=>"Paid","name"=>"paid","callback_php"=>'($row->paid == 1? "Yes" : "No")'];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Frame','name'=>'frames_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'frames,name'];
		$this->form[] = ['label'=>'Cms User','name'=>'cms_users_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
		$this->form[] = ['label'=>'Status','name'=>'status_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'reservation_statuses,name'];
		$this->form[] = ['label'=>'Paid','name'=>'paid','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','dataenum'=>'1|YES;0|NO'];

		$daysColumns[] = ['label'=>'Day','name'=>'day','type'=>'text'];
		$this->form[] = ['label'=>'Reservation days','name'=>'reservation_rows','type'=>'child','columns'=>$daysColumns,'table'=>'reservation_rows','foreign_key'=>'reservation_id'];

		$serviceColumns[] = ['label'=>'Service','name'=>'service_id','type'=>'datamodal','datamodal_table'=>'services','datamodal_columns'=>'name,price','datamodal_size'=>'large'];

		$this->form[] = ['label'=>'Reservation services','name'=>'reservation_services','type'=>'child','columns'=>$serviceColumns,'table'=>'reservation_services','foreign_key'=>'reservation_id'];
		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ['label'=>'Frame','name'=>'frames_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'frames,name'];
		//$this->form[] = ['label'=>'Cms User','name'=>'cms_users_id','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
		//$this->form[] = ['label'=>'Status','name'=>'status_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'status,name'];
		//$this->form[] = ['label'=>'Paid','name'=>'paid','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','dataenum'=>'1|YES;0|NO'];
		# OLD END FORM

		/*
		| ----------------------------------------------------------------------
		| Sub Module
		| ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
		|
		*/
		$this->sub_module = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Action Button / Menu
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
		| @icon        = Font awesome class icon. e.g : fa fa-bars
		| @color 	   = Default is primary. (primary, warning, succecss, info)
		| @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
		|
		*/
		$this->addaction = array();
		$this->addaction[] = ['label'=>'Addebita la prenotazione','url'=>CRUDBooster::mainpath('charge-reservation/[id]'),'icon'=>'fa fa-dollar','color'=>'info','showIf'=>"[paid] == '0'", 'confirmation' => true];



		/*
		| ----------------------------------------------------------------------
		| Add More Button Selected
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @icon 	   = Icon from fontawesome
		| @name 	   = Name of button
		| Then about the action, you should code at actionButtonSelected method
		|
		*/
		$this->button_selected = array();


		/*
		| ----------------------------------------------------------------------
		| Add alert message to this module at overheader
		| ----------------------------------------------------------------------
		| @message = Text of message
		| @type    = warning,success,danger,info
		|
		*/
		$this->alert        = array();



		/*
		| ----------------------------------------------------------------------
		| Add more button to header button
		| ----------------------------------------------------------------------
		| @label = Name of button
		| @url   = URL Target
		| @icon  = Icon from Awesome.
		|
		*/
		$this->index_button = array();



		/*
		| ----------------------------------------------------------------------
		| Customize Table Row Color
		| ----------------------------------------------------------------------
		| @condition = If condition. You may use field alias. E.g : [id] == 1
		| @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
		|
		*/
		$this->table_row_color = array();


		/*
		| ----------------------------------------------------------------------
		| You may use this bellow array to add statistic at dashboard
		| ----------------------------------------------------------------------
		| @label, @count, @icon, @color
		|
		*/
		$this->index_statistic = array();



		/*
		| ----------------------------------------------------------------------
		| Add javascript at body
		| ----------------------------------------------------------------------
		| javascript code in the variable
		| $this->script_js = "function() { ... }";
		|
		*/
		$this->script_js = NULL;


		/*
		| ----------------------------------------------------------------------
		| Include HTML Code before index table
		| ----------------------------------------------------------------------
		| html code to display it before index table
		| $this->pre_index_html = "<p>test</p>";
		|
		*/
		$this->pre_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include HTML Code after index table
		| ----------------------------------------------------------------------
		| html code to display it after index table
		| $this->post_index_html = "<p>test</p>";
		|
		*/
		$this->post_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include Javascript File
		| ----------------------------------------------------------------------
		| URL of your javascript each array
		| $this->load_js[] = asset("myfile.js");
		|
		*/
		$this->load_js = array();



		/*
		| ----------------------------------------------------------------------
		| Add css style at body
		| ----------------------------------------------------------------------
		| css code in the variable
		| $this->style_css = ".style{....}";
		|
		*/
		$this->style_css = NULL;



		/*
		| ----------------------------------------------------------------------
		| Include css File
		| ----------------------------------------------------------------------
		| URL of your css each array
		| $this->load_css[] = asset("myfile.css");
		|
		*/
		$this->load_css = array();


	}

	public function getChargeReservation($id){
		$reservation = DB::table('reservations')->where('id',$id)->first();
		$errors = [];
		$card = json_decode($reservation->cc,1);

		$testKey = "sk_test_ZzG8kiV7422JCVKLFEIfNa7P00eBIt21yo";
		$productionKey = "sk_live_Ne0uqoSq1aD2lrcU6kEIaH9O00vxHscbKv";

		$key = $productionKey;

		$stripe = Stripe::make($key);



		try{

		$charge = $stripe->charges()->capture($reservation->cc);

		if($charge['status'] == 'succeeded') {
			DB::table('reservations')->where('id',$id)->update(['paid'=>1]);
		}
	}
	catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
		$errors[] =  $e->getMessage();
	}
	catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
		$errors[] =  $e->getMessage();
	}

	if(count($errors)>0){

		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],implode(',',$errors),"danger");
	}else{


		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Il pagamento è andato a buon fine","success");

	}

}

/*
| ----------------------------------------------------------------------
| Hook for button selected
| ----------------------------------------------------------------------
| @id_selected = the id selected
| @button_name = the name of button
|
*/
public function actionButtonSelected($id_selected,$button_name) {
	//Your code here

}


/*
| ----------------------------------------------------------------------
| Hook for manipulate query of index result
| ----------------------------------------------------------------------
| @query = current sql query
|
*/
public function hook_query_index(&$query) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for manipulate row of index table html
| ----------------------------------------------------------------------
|
*/
public function hook_row_index($column_index,&$column_value) {
	//Your code here
}

/*
| ----------------------------------------------------------------------
| Hook for manipulate data input before add data is execute
| ----------------------------------------------------------------------
| @arr
|
*/
public function hook_before_add(&$postdata) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for execute command after add public static function called
| ----------------------------------------------------------------------
| @id = last insert id
|
*/
public function hook_after_add($id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for manipulate data input before update data is execute
| ----------------------------------------------------------------------
| @postdata = input post data
| @id       = current id
|
*/
public function hook_before_edit(&$postdata,$id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for execute command after edit public static function called
| ----------------------------------------------------------------------
| @id       = current id
|
*/
public function hook_after_edit($id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for execute command before delete public static function called
| ----------------------------------------------------------------------
| @id       = current id
|
*/
public function hook_before_delete($id) {
	//Your code here

}

/*
| ----------------------------------------------------------------------
| Hook for execute command after delete public static function called
| ----------------------------------------------------------------------
| @id       = current id
|
*/
public function hook_after_delete($id) {
	//Your code here

}

public function SetReservation(Request $request){
	$input = $request->all();
	$days = json_decode($input['reservation']['days'],1);
	$services = json_decode($input['reservation']['services'],1);
	$cc = $input['cc'];
	if($cc['num'] == '' || $cc['ccv'] == '' || $cc['exp_mm'] == '' || $cc['exp_yy'] == ''){
		return ['status' => 0,'message' => 'Controlla i dati della carta di credito'];
	}
	#$cc = json_encode($input['cc']);
	$cc = $input['cc'];


	#CARD TOKENIZATION


	$testKey = "sk_test_ZzG8kiV7422JCVKLFEIfNa7P00eBIt21yo";
	$productionKey = "sk_live_Ne0uqoSq1aD2lrcU6kEIaH9O00vxHscbKv";

	$key = $productionKey;

	$stripe = Stripe::make($key);
	try {
		$token = $stripe->tokens()->create([
			'card' => [
				'number' => $cc['num'],
				'exp_month' => $cc['exp_mm'],
				'exp_year' => $cc['exp_yy'],
				'cvc' => $cc['ccv'],
			],
		]);

		$charge = $stripe->charges()->create([
			'card' => $token['id'],
			'currency' => 'EUR',
			'amount' => $input['reservation']['total_price'],
			'description' => 'Reservation ',
			"capture" => false, #<---------- preautorizzazione
		]);

		if($charge['status'] == 'succeeded') {

		}else{
			return ['status'=>0];
		}

		$reservationId = DB::table('reservations')->insertGetId([
			'created_at' => date('Y-m-d H:i:s'),
			'frames_id' => $input['reservation']['frames_id'],
			'cms_users_id' => $input['reservation']['cms_users_id'],
			'status_id' => 1,
			'paid' => 0,
			'total_price' => $input['reservation']['total_price'],
			'cc' => $charge['id']
		]);


		##################

		if(count($services)>0){
			foreach($services as $serviceId){
				DB::table('reservation_services')->insert([
					'reservation_id' => $reservationId,
					'service_id' => $serviceId
				]);
			}
		}
		if(count($days)>0){
			foreach ($days as $day) {
				DB::table('reservation_rows')->insert([
					'reservation_id' => $reservationId,
					'day' => date('Y-m-d',strtotime($day))
				]);
			}
		}

		#Send reservation notification

		$data = [];
		CRUDBooster::sendEmail(['to'=>'giojkd@gmail.com','data'=>$data,'template'=>'new_reservation']);

		##############################

		return ['status'=>1,'reservation_id'=>$reservationId];
	}
	catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
		$errors[] =  $e->getMessage();
		return ['status'=>0];
	}
	catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
		$errors[] =  $e->getMessage();
		return ['status'=>0];
	}
}

public function GetFrameBusyDays(Request $request){
	$input = $request->all();
	$return = [];
	$frame = DB::table('frames')->where('id',$input['id'])->first();
	$return['price'] = $frame->daily_cost;
	$busyDaysTemp = DB::table('reservation_rows')
	->select('reservation_rows.day')
	->leftJoin('reservations','reservation_rows.reservation_id','=','reservations.id')
	->where('reservations.frames_id',$input['id'])
	->get();

	if(!$busyDaysTemp->isEmpty()){
		foreach($busyDaysTemp as $day){
			$return['dates'][] = date('Ymd',strtotime($day->day));
		}
	}
	$return['dates'] = [];
	return $return;
}

function DeleteReservation(Request $request){
	$input = $request->all();
	DB::table('reservations')->where('id',$input['id'])->delete();
	return ['status'=>1];
}



//By the way, you can still create your own method in here... :)


}
