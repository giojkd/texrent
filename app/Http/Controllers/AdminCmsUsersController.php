<?php namespace App\Http\Controllers;

use Session;
#use Request;
use Illuminate\Http\Request;
use DB;
use CRUDbooster;
use Illuminate\Support\Facades\Hash;

class AdminCmsUsersController extends \crocodicstudio\crudbooster\controllers\CBController {


	public function cbInit() {
		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->table               = 'cms_users';
		$this->primary_key         = 'id';
		$this->title_field         = "name";
		$this->button_action_style = 'button_icon';
		$this->button_import 	   = FALSE;
		$this->button_export 	   = FALSE;
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = array();
		$this->col[] = array("label"=>"Name","name"=>"name");
		$this->col[] = array("label"=>"Surname","name"=>"surname");
		$this->col[] = array("label"=>"Company","name"=>"company");
		$this->col[] = array("label"=>"Telephone","name"=>"telephone");
		$this->col[] = array("label"=>"Email","name"=>"email");
		$this->col[] = array("label"=>"Privilege","name"=>"id_cms_privileges","join"=>"cms_privileges,name");
		$this->col[] = array("label"=>"Photo","name"=>"photo","image"=>1);
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = array();
		$this->form[] = array("label"=>"Name","name"=>"name",'required'=>false,'validation'=>'alpha_spaces|min:3');
		$this->form[] = array("label"=>"Surname","name"=>"surname",'required'=>false,'validation'=>'alpha_spaces|min:3');
		$this->form[] = array("label"=>"Company","name"=>"company",'required'=>false,'validation'=>'alpha_spaces|min:3');
		$this->form[] = array("label"=>"Telephone","name"=>"telephone",'required'=>false,'validation'=>'alpha_spaces|min:3');
		$this->form[] = array("label"=>"Email","name"=>"email",'required'=>true,'type'=>'email','validation'=>'required|email|unique:cms_users,email,'.CRUDBooster::getCurrentId());
		$this->form[] = array("label"=>"Photo","name"=>"photo","type"=>"upload","help"=>"Recommended resolution is 200x200px",'required'=>false,'validation'=>'image|max:1000','resize_width'=>90,'resize_height'=>90);
		$this->form[] = array("label"=>"Privilege","name"=>"id_cms_privileges","type"=>"select","datatable"=>"cms_privileges,name",'required'=>true);
		// $this->form[] = array("label"=>"Password","name"=>"password","type"=>"password","help"=>"Please leave empty if not change");
		$this->form[] = array("label"=>"Password","name"=>"password","type"=>"password","help"=>"Please leave empty if not change");
		$this->form[] = array("label"=>"Password Confirmation","name"=>"password_confirmation","type"=>"password","help"=>"Please leave empty if not change");
		# END FORM DO NOT REMOVE THIS LINE

	}

	public function getProfile() {

		$this->button_addmore = FALSE;
		$this->button_cancel  = FALSE;
		$this->button_show    = FALSE;
		$this->button_add     = FALSE;
		$this->button_delete  = FALSE;
		$this->hide_form 	  = ['id_cms_privileges'];

		$data['page_title'] = trans("crudbooster.label_button_profile");
		$data['row']        = CRUDBooster::first('cms_users',CRUDBooster::myId());
		$this->cbView('crudbooster::default.form',$data);
	}
	public function hook_before_edit(&$postdata,$id) {
		unset($postdata['password_confirmation']);
	}
	public function hook_before_add(&$postdata) {
		unset($postdata['password_confirmation']);
	}

	public function UserEdit(Request $request){
		$input = $request->all();
		$data = $input['data'];
		$where = $input['where'];
		if($data['password']!=''){
				$data['password'] = Hash::make($data['password']);
		}else{
			unset($data['password']);
		}

		DB::table('cms_users')->where('id',$where['id'])->update($data);

		return $input;
	}

	public function SignIn(Request $request){

		$return = ['status' => 0];
		$input = $request->all();
		#$input['password'] = Hash::make($input['password']);
		$user = DB::table('cms_users')
		->where('email',$input['email'])
		->where('status','Active')
		->where('id_cms_privileges',2)
		->first();





		if (Hash::check($input['password'], $user->password)) {

			if($user != NULL){
				$userReturn = [
					'name' => $user->name,
					'surname' => $user->surname,
					'email' => $user->email,
					'company' => $user->company,
					'telephone' => $user->telephone,
				];
				$return = [
					'status' => 1,
					'id' => $user->id,
					'user' => $userReturn
				];

			}
		}
		return $return;
	}


	public function SignUp(Request $request){
		$input = $request->all();
		$data = $input['data'];
		$data['password'] = Hash::make($data['password']);
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['id_cms_privileges'] = 2;
		$data['status'] = 'Active';

		$UserId = DB::table('cms_users')->insertGetId($data);
		unset($data['password']);
		$return = [
			'id' => $UserId,
			'user' => $data,
			'status' => 1
		];
		return $return;
	}

	public function getUser(Request $request){
		$input= $request->all();
		$reservations = [];
		$reservations_ =
		DB::table('reservations')
		->select('reservations.id as id','reservations.total_price as total','frames.name as name')
		->leftJoin('frames','reservations.frames_id','=','frames.id')
		->where('cms_users_id',$input['id'])->get();
		if(!$reservations_->isEmpty()){
			foreach($reservations_ as $index => $reservation){
				$reservations[$index] = $reservation;
				$reservations[$index]->days  = DB::table('reservation_rows')->select('day')->where('reservation_id',$reservation->id)->get();
			}
		}
		$return = [
			'reservations' => $reservations
		];
		return $return;
	}

}
