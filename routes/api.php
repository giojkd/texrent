<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/sign-up','AdminCmsUsersController@SignUp');
Route::get('/sign-in','AdminCmsUsersController@SignIn');

Route::get('/set-reservation','AdminReservationsController@SetReservation');

Route::get('/get-articles-list','AdminArticlesController@GetArticles');
Route::get('/get-article-details','AdminArticlesController@GetArticleDetails');

Route::get('/frames','AdminFramesController@getFrames');

Route::get('/get-weaving-mills','AdminWeavingMillsController@getWeavingMills');
Route::get('/get-weaving-mill-details','AdminWeavingMillsController@getWeavingMillDetails');

Route::get('/get-frame-busy-days','AdminReservationsController@GetFrameBusyDays');

Route::get('/get-services','AdminServicesController@getServices');

Route::get('/user-edit','AdminCmsUsersController@UserEdit');
Route::get('/get-user-details','AdminCmsUsersController@getUser');
Route::get('/delete-reservation','AdminReservationsController@DeleteReservation');
